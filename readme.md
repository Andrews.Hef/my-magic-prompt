
# Project Title

MY-MAGIC-PROMPT is a simple PROMPT create in class to discover bash script





## Features


Voici une description point par point des commandes que vous souhaitez inclure dans ce prompre:

- help : Cette commande affichera une liste des commandes disponibles dans votre prompt, avec une brève description de chacune.

- ls : Permet de lister les fichiers et dossiers visibles, y compris les fichiers et dossiers cachés.

- rm : Utilisé pour supprimer un fichier spécifié.

- rmd ou rmdir : Utilisé pour supprimer un dossier spécifié.

- about : Affiche une description de votre programme ou prompt interactif.

- version ou --v ou vers : Affiche la version de votre prompt interactif.

- age : Demande à l'utilisateur de saisir son âge, puis indique s'il est majeur ou mineur.

- quit : Permet à l'utilisateur de quitter le prompt interactif.

- profil : Affiche toutes les informations sur l'utilisateur, notamment le prénom, le nom, l'âge et l'adresse e-mail.

- passw : Permet de changer le mot de passe, en demandant une confirmation.

- cd : Utilisé pour naviguer dans un dossier spécifié ou pour revenir au dossier précédent.

- pwd : Affiche le répertoire de travail actuel.

- hour : Affiche l'heure actuelle.

* : Indique une commande inconnue lorsque l'utilisateur entre une commande invalide.

- httpget : Permet de télécharger le code source HTML d'une page web et de l'enregistrer dans un fichier spécifié, en demandant le nom du fichier à l'utilisateur.

- smtp : Permet à l'utilisateur d'envoyer un courrier électronique en spécifiant une adresse e-mail, un sujet et le corps du message.

- open : Utilisé pour ouvrir un fichier directement dans l'éditeur VIM, même si le fichier n'existe pas.

## Feedback

If you have any feedback,please create issue 


## Deployment

To deploy this project need a ubuntu terminal and use 

```bash
  bash main.sh
```

