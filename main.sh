#!/bin/bash
source quit.sh
source hour.sh
source age.sh
source about.sh
source pwd.sh
source ls.sh
source rm.sh
source rmdir.sh
source profil.sh
source version.sh
source vim.sh
source cd.sh
source passw.sh
source httpget.sh
source help.sh
source smtp.sh

#command instead of bin/[command]
cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    quit | exit ) quit;;
    hour ) hour;;
    age ) age;;
    about ) about;;
    pwd) current_directory;;
    ls) lsa $2 $3;;
    rm ) rm $2 $3;;
    rmdir| rmd ) rmdir $2 $3;;
    profil ) profil $pseudo $email ;;
    version | --vers |-v ) version ;;
    open ) write $2 ;;
    cd ) change $2 ;;
    passw ) changePassword $pass ;;
    httpget ) htmlGet $2;; 
    help ) needHelp;;
    smtp) smtp ;;
    *) echo "commande pas reconnu";;

  esac
}

main() {
  lineCount=1
  logInfo=()
  logInfo+=("Andrews")
  logCounter=1

  while [ $logCounter -le 1 ]; do 
    echo "rentrez votre pseudo?"
    read pseudo 

    echo "rentres votre email?"
    read email

    echo "rentrez votre mot de passe"
    read -s pass


    logCounter=$(($logCounter+1))
  done
  echo $pass $pseudo $email

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mAndrews\033[m ~ ☠️ ~ "
    read string

    cmd $string 
    lineCount=$(($lineCount+1))
  done
}

main
