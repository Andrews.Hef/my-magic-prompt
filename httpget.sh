htmlGet(){

    echo "nom du fichier ou telecharger le code HTML:(ex:filename.txt)"
    read filename


    # Utilisation de curl pour télécharger la page HTML
    curl -s "$1" -o "$filename"

    # Vérification si la requête a réussi
    if [ $? -eq 0 ]; then
        echo "Le code HTML a été téléchargé avec succès dans $filename."
    else
        echo "La requête a échoué."
    fi
}